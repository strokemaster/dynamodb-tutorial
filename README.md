# README #


### What is this repository for? ###

* This is to showcase connecting to DynamoDB, running insert statements and executing queries.  This is a baseline to use when writing more complicated statements.  

### How do I get set up? ###

* Install this: http://aws.amazon.com/eclipse/ **Note:  You need to have the J2EE version of Eclipse or you may run into issues.**
* Read about DynamoDB: http://aws.amazon.com/dynamodb/ and go through http://aws.amazon.com/dynamodb/getting-started/.  Not necessary to create the tables, but it's important to understand how tables are created and what things you can / can't query. 
View more details on Confluence: https://strokemaster.atlassian.net/wiki/display/STROK/Database+Reading+Materials
* Clone this repo.


### Contribution guidelines ###

This is just for show.  Play around and do whatever you want.  

### Who do I talk to? ###

Talk to Sean if you have any questions.