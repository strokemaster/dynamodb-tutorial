
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;

public class GettingStartedTryQuery {


    static AmazonDynamoDBClient client = new AmazonDynamoDBClient(new ProfileCredentialsProvider());
    static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    static Region uswest2 = Region.getRegion(Regions.US_WEST_2);
    static String USER_TABLE="Users";
    
    public static void main(String[] args) throws Exception {

        try {
        
        client.setRegion(uswest2);	
        //addUser("trstaake@gmail.com", "test2");
        getUser("sbaxter323@gmail.com", "test1");
        }  
        catch (AmazonServiceException ase) {
            System.err.println(ase.getMessage());
        }  
    }
    
    // Used for getting a user object from the DB based on email
    // Check email and password against what is in the database, return user values if true
    private static void getUser(String email, String password) {
        
        Map<String, AttributeValue> lastEvaluatedKey = null;
        do {
            
        	// Compare the hash key for the Users table against the input String
            Condition hashKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ.toString())
                .withAttributeValueList(new AttributeValue().withS(email));
            
            // Required for tables with Hash and Range keys
            /*Condition rangeKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.GT.toString())
                .withAttributeValueList(new AttributeValue().withS(twoWeeks));*/
            
            Map<String, Condition> keyConditions = new HashMap<String, Condition>();
            keyConditions.put("Email", hashKeyCondition);
            
            QueryRequest queryRequest = new QueryRequest().withTableName(USER_TABLE)
                .withKeyConditions(keyConditions)
                .withProjectionExpression("Email, Password")
                .withLimit(1).withExclusiveStartKey(lastEvaluatedKey);   
            
           QueryResult result = client.query(queryRequest);
            
            for (Map<String, AttributeValue> item : result.getItems()) {
            	System.out.println("DB: " + item.get("Password") + " Client: " + password);
                if (item.get("Password").getS().equals(password))
                	System.out.println("Passwords match");
            }
            lastEvaluatedKey = result.getLastEvaluatedKey();
        } while (lastEvaluatedKey != null);        
    }
    
    private static void addUser(String email, String password) {
        
        try {
        	
            // Add user
            Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
            item.put("Email", new AttributeValue().withS(email));
            item.put("Password", new AttributeValue().withS(password));
            PutItemRequest itemRequest = new PutItemRequest().withTableName(USER_TABLE).withItem(item);
            client.putItem(itemRequest);
            item.clear();
                
        }   catch (AmazonServiceException ase) {
            System.err.println("Failed to create item in " + USER_TABLE + " " + ase);
        } 

    }
    
    private static void findRepliesInLast15DaysWithConfig(String tableName, String forumName, String threadSubject) {

        String replyId = forumName + "#" + threadSubject;
        long twoWeeksAgoMilli = (new Date()).getTime() - (15L*24L*60L*60L*1000L);
        Date twoWeeksAgo = new Date();
        twoWeeksAgo.setTime(twoWeeksAgoMilli);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String twoWeeksAgoStr = df.format(twoWeeksAgo);
        
        Map<String, AttributeValue> lastEvaluatedKey = null;
        do {
            
            Condition hashKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ.toString())
                .withAttributeValueList(new AttributeValue().withS(replyId));
            
            Condition rangeKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.GT.toString())
                .withAttributeValueList(new AttributeValue().withS(twoWeeksAgoStr));
            
            Map<String, Condition> keyConditions = new HashMap<String, Condition>();
            keyConditions.put("Id", hashKeyCondition);
            keyConditions.put("ReplyDateTime", rangeKeyCondition);
            
            QueryRequest queryRequest = new QueryRequest().withTableName(tableName)
                .withKeyConditions(keyConditions)
                .withProjectionExpression("Message, ReplyDateTime, PostedBy")
                .withLimit(1).withExclusiveStartKey(lastEvaluatedKey);   
            
           QueryResult result = client.query(queryRequest);
            for (Map<String, AttributeValue> item : result.getItems()) {
                printItem(item);
            }
            lastEvaluatedKey = result.getLastEvaluatedKey();
        } while (lastEvaluatedKey != null);        
    }
  
    private static void printItem(Map<String, AttributeValue> attributeList) {
        for (Map.Entry<String, AttributeValue> item : attributeList.entrySet()) {
            String attributeName = item.getKey();
            AttributeValue value = item.getValue();
            System.out.println(attributeName + " "
                    + (value.getS() == null ? "" : "S=[" + value.getS() + "]")
                    + (value.getN() == null ? "" : "N=[" + value.getN() + "]")
                    + (value.getB() == null ? "" : "B=[" + value.getB() + "]")
                    + (value.getSS() == null ? "" : "SS=[" + value.getSS() + "]")
                    + (value.getNS() == null ? "" : "NS=[" + value.getNS() + "]")
                    + (value.getBS() == null ? "" : "BS=[" + value.getBS() + "] \n"));
        }
    }
}
      